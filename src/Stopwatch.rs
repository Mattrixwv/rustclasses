

enum TimeResolution{NANOSECOND, MICROSECOND, MILLISECOND, SECOND, MINUTE, HOUR, SIZE}

pub struct Stopwatch{
	startTime: std::time::Instant,
	stopTime: std::time::Instant,
	hasStarted: bool,
	hasStopped: bool,
}
impl Stopwatch{
	//Constructor. Returns a new, blank Stopwatch
	pub fn new() -> Stopwatch{
		let timer = Stopwatch{startTime: std::time::Instant::now(), stopTime: std::time::Instant::now(), hasStarted: false, hasStopped: false};
		timer
	}
	//Returns a long with the elapsed time in nanoseconds. Used by other functions to get the time before converting it to the correct resolution
	fn getTime(&self) -> std::time::Duration{
		//If the Stopwatch hasn't been started yet
		if(!self.hasStarted){
			//TODO: This should throw an exception instead of returning 0
			return std::time::Duration::new(0, 0);
		}
		//If the Stopwatch was started but not stopped use the current time as stopTime
		else if(!self.hasStopped){
			return std::time::Instant::now().duration_since(self.startTime);
		}
		//If the Stopwatch was started and stopped
		else{
			return self.stopTime.duration_since(self.startTime);
		}
	}
	//Start the Stopwatch
	//Because of how this is implemented if one restarts the stopwatch it resets itself
	pub fn start(&mut self){
		//Start the Stopwatch
		self.hasStarted = true;
		//It hasn't been stopped yet
		self.hasStopped = false;
		//Put this last to ensure that the time recorded is as close to the return time as possible
		self.startTime = std::time::Instant::now();
	}
	pub fn stop(&mut self){
		//Put this first to ensure the time recorded is as close to the call time as possible
		let tempTime = std::time::Instant::now();
		//Make sure that the Stopwatch has started before it can be stopped
		if(self.hasStarted && !self.hasStopped){
			//Set the stop time
			self.stopTime = tempTime;
			//Record that stop has been called
			self.hasStopped = true;
		}
		//If the stopwatch hasn't been started throw an exception
		else if(!self.hasStarted){
			//TODO: An exception should be thrown here
		}
		//If the stopwatch has already been stopped throw an exception
		else if(self.hasStopped){
			//TODO: An exception should be thrown here
		}
	}
	pub fn getNano(&self) -> u128{
		return self.getTime().as_nanos();
	}
	pub fn getMicro(&self) -> u128{
		return self.getTime().as_micros();
	}
	pub fn getMilli(&self) -> u128{
		return self.getTime().as_millis();
	}
	pub fn getSeconds(&self) -> u64{
		return self.getTime().as_secs();
	}
	pub fn getMinutes(&self) -> f64{
		return self.getTime().as_secs_f64() / 60.0;
	}
	pub fn getHours(&self) -> f64{
		return self.getTime().as_secs_f64() / 3600.0;
	}
	pub fn getString(&self) -> String{
		//Get the time in the most granular form possible
		return Stopwatch::getStr(self.getNano() as f64);
	}
	//A function to print out the time for any nanosecond duration passed to it
	pub fn getStr(nano: f64) -> String{
		let mut duration = nano;
		let mut timeRes = TimeResolution::NANOSECOND as i32;
		//Reduce the number until it has the appropriate number of digits. (xxx.xxx)
		//This loop works down to seconds
		while((timeRes < TimeResolution::SECOND as i32) && (duration >= 1000.0)){
			duration /= 1000.0;
			timeRes += 1;
		}
		//Check if the duration needs reduced to minutes
		if((duration >= 120.0) && (timeRes == TimeResolution::SECOND as i32)){
			duration /= 60.0;
			timeRes = TimeResolution::MINUTE as i32;

			//Check if the duration needs reduced to hours
			if(duration >= 60.0){
				duration /= 60.0;
				timeRes = TimeResolution::HOUR as i32;
			}
		}

		//Turn the number into a string
		let mut time = format!("{:0.3} ", duration);
		//Doing this with an if/else if statement because case doesn't exist and match doesn't do what I need it to
		if(timeRes == TimeResolution::NANOSECOND as i32){
			time += "nanoseconds";
		}
		else if(timeRes == TimeResolution::MICROSECOND as i32){
			time += "microseconds";
		}
		else if(timeRes == TimeResolution::MILLISECOND as i32){
			time += "milliseconds";
		}
		else if(timeRes == TimeResolution::SECOND as i32){
			time += "seconds";
		}
		else if(timeRes == TimeResolution::MINUTE as i32){
			time += "minutes";
		}
		else if(timeRes == TimeResolution::HOUR as i32){
			time += "hours";
		}
		else{
			time = "There was an error in getStr".to_string();
		}

		return time;
	}
	pub fn reset(&mut self){
		self.hasStarted = false;
		self.hasStopped = false;
		//Set the times equal to each other for a duration of 0
		self.startTime = std::time::Instant::now();
		self.stopTime = self.startTime;
	}
	pub fn to_string(&self) -> String{
		return self.getString();
	}
}
impl std::fmt::Display for Stopwatch{
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result{
		write!(f, "{}", self.getString())
	}
}
