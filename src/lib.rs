#![allow(non_snake_case)]
#![allow(unused_parens)]
#![allow(dead_code)]


extern crate num;
extern crate assert_approx_eq;

pub mod Stopwatch;
pub mod Algorithms;


#[cfg(test)]
mod AlgorithmsTests{
	#[test]
	fn testGetAllFib(){
		//Test 1
		let mut correctAnswer1 = Vec::new();
		correctAnswer1.extend_from_slice(&[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]);
		let highestNumber1 = 100;
		let answer1 = super::Algorithms::getAllFib(highestNumber1);
		assert_eq!(correctAnswer1, answer1);

		//Test 2
		let mut correctAnswer2 = Vec::new();
		correctAnswer2.extend_from_slice(&[1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]);
		let highestNumber2 = 1000;
		let answer2 = super::Algorithms::getAllFib(highestNumber2);
		assert_eq!(correctAnswer2, answer2);

		//Test3
		let mut correctAnswerBig = Vec::new();
		correctAnswerBig.extend_from_slice(&[num::BigInt::from(1), num::BigInt::from(1), num::BigInt::from(2), num::BigInt::from(3), num::BigInt::from(5), num::BigInt::from(8), num::BigInt::from(13), num::BigInt::from(21), num::BigInt::from(34), num::BigInt::from(55), num::BigInt::from(89)]);
		let highestNumberBig = num::BigInt::from(100);
		let answerBig = super::Algorithms::getAllFibBig(highestNumberBig);
		assert_eq!(correctAnswerBig, answerBig);
	}
	#[test]
	fn testGetFib(){
		//Test1
		let correctAnswer1 = 144;
		let number1 = 12;
		let answer1 = super::Algorithms::getFib(number1);
		assert_eq!(correctAnswer1, answer1);
		//Test2
		let correctAnswer2 = 6765;
		let number2 = 20;
		let answer2 = super::Algorithms::getFib(number2);
		assert_eq!(correctAnswer2, answer2);

		//Test3
		let correctAnswer3 = "1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816".parse::<num::BigInt>().unwrap();
		let number3 = num::BigInt::from(4782);
		let answer3 = super::Algorithms::getFibBig(number3);
		assert_eq!(correctAnswer3, answer3);
	}
	#[test]
	fn testGetPrimes(){
		//Test1
		let mut correctAnswer1 = Vec::<i64>::new();
		correctAnswer1.extend_from_slice(&[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]);
		let topNum1 = 100;
		let answer1 = super::Algorithms::getPrimes(topNum1);
		assert_eq!(correctAnswer1, answer1);
		//Test2
		let mut correctAnswer2 = Vec::<num::BigInt>::new();
		correctAnswer2.extend_from_slice(&[num::BigInt::from(2), num::BigInt::from(3), num::BigInt::from(5), num::BigInt::from(7), num::BigInt::from(11), num::BigInt::from(13), num::BigInt::from(17), num::BigInt::from(19), num::BigInt::from(23), num::BigInt::from(29), num::BigInt::from(31), num::BigInt::from(37), num::BigInt::from(41), num::BigInt::from(43), num::BigInt::from(47), num::BigInt::from(53), num::BigInt::from(59), num::BigInt::from(61), num::BigInt::from(67), num::BigInt::from(71), num::BigInt::from(73), num::BigInt::from(79), num::BigInt::from(83), num::BigInt::from(89), num::BigInt::from(97)]);
		let topNum2 = num::BigInt::from(100);
		let answer2 = super::Algorithms::getPrimesBig(topNum2);
		assert_eq!(correctAnswer2, answer2);
	}
	#[test]
	fn testGetFactors(){
		//Test1
		let mut correctAnswer1 = Vec::<i64>::new();
		correctAnswer1.extend_from_slice(&[2, 2, 5, 5]);
		let number1 = 100;
		let answer1 = super::Algorithms::getFactors(number1);
		assert_eq!(correctAnswer1, answer1);
		//Test2
		let mut correctAnswer2 = Vec::<i64>::new();
		correctAnswer2.extend_from_slice(&[2, 7, 7]);
		let number2 = 98;
		let answer2 = super::Algorithms::getFactors(number2);
		assert_eq!(correctAnswer2, answer2);
		//Test3
		let mut correctAnswer3 = Vec::<num::BigInt>::new();
		correctAnswer3.extend_from_slice(&[num::BigInt::from(2), num::BigInt::from(2), num::BigInt::from(5), num::BigInt::from(5)]);
		let number3 = num::BigInt::from(100);
		let answer3 = super::Algorithms::getFactorsBig(number3);
		assert_eq!(correctAnswer3, answer3);
	}
	#[test]
	fn testGetNumPrimes(){
		//Test 1
		let mut correctAnswer1 = Vec::<i64>::new();
		correctAnswer1.extend_from_slice(&[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]);
		let numPrimes1 = 25;
		let answer1 = super::Algorithms::getNumPrimes(numPrimes1);
		assert_eq!(correctAnswer1, answer1);

		//Test 2
		let mut correctAnswer2 = Vec::<num::BigInt>::new();
		correctAnswer2.extend_from_slice(&[num::BigInt::from(2), num::BigInt::from(3), num::BigInt::from(5), num::BigInt::from(7), num::BigInt::from(11), num::BigInt::from(13), num::BigInt::from(17), num::BigInt::from(19), num::BigInt::from(23), num::BigInt::from(29), num::BigInt::from(31), num::BigInt::from(37), num::BigInt::from(41), num::BigInt::from(43), num::BigInt::from(47), num::BigInt::from(53), num::BigInt::from(59), num::BigInt::from(61), num::BigInt::from(67), num::BigInt::from(71), num::BigInt::from(73), num::BigInt::from(79), num::BigInt::from(83), num::BigInt::from(89), num::BigInt::from(97)]);
		let numPrimes2 = num::BigInt::from(25);
		let answer2 = super::Algorithms::getNumPrimesBig(numPrimes2);
		assert_eq!(correctAnswer2, answer2);
	}
	#[test]
	fn testGetDivisors(){
		//Test1
		let mut correctAnswer1 = Vec::<i64>::new();
		correctAnswer1.extend_from_slice(&[1, 2, 4, 5, 10, 20, 25, 50, 100]);
		let topNum1 = 100;
		let answer1 = super::Algorithms::getDivisors(topNum1);
		assert_eq!(correctAnswer1, answer1);

		//Test2
		let mut correctAnswer2 = Vec::<num::BigInt>::new();
		correctAnswer2.extend_from_slice(&[num::BigInt::from(1), num::BigInt::from(2), num::BigInt::from(4), num::BigInt::from(5), num::BigInt::from(10), num::BigInt::from(20), num::BigInt::from(25), num::BigInt::from(50), num::BigInt::from(100)]);
		let topNum2 = num::BigInt::from(100);
		let answer2 = super::Algorithms::getDivisorsBig(topNum2);
		assert_eq!(correctAnswer2, answer2);
	}
	#[test]
	fn testGetPermutations(){
		//Test
		let permString = "012".to_string();
		let mut correctAnswer = Vec::<String>::new();
		correctAnswer.extend_from_slice(&["012".to_string(), "021".to_string(), "102".to_string(), "120".to_string(), "201".to_string(), "210".to_string()]);
		let answer = super::Algorithms::getPermutations(permString);
		println!("answer: {:?}", answer);
		assert_eq!(correctAnswer, answer);
	}
	#[test]
	fn testSwapString(){
		//Test 1
		let string = "012".to_string();
		let correctAnswer = "210".to_string();
		let answer = super::Algorithms::swapString(string, 0, 2);
		println!("swaped: {}", answer);
		assert_eq!(correctAnswer, answer);
	}
}


#[cfg(test)]
mod StopwatchTests{
	#[test]
	fn testStartStop(){
		let mut timer = super::Stopwatch::Stopwatch::new();
		timer.start();
		timer.stop();
		//If it gets to here without panicing everything went well
	}
	#[test]
	fn testConversion(){
		let mut timer = super::Stopwatch::Stopwatch::new();
		let mut sum = 0i64;
		//Start the timer
		timer.start();
		//Do something to run some time
		for cnt in 0..100_000{
			sum += cnt;
		}
		//Stop the timer
		timer.stop();
		//Asser something so the sum isn't ignored during compile
		assert_ne!(sum, 0);
		//Check that the different resolutions work out correctly
		let nano = timer.getNano();
		assert_eq!(timer.getMicro(), (nano / 1000));
		assert_eq!(timer.getMilli(), (nano / 1000000));
		assert_eq!(timer.getSeconds(), (nano / 1000000000) as u64);
		super::assert_approx_eq::assert_approx_eq!(timer.getMinutes(), (nano as f64 / 60000000000.0));
		super::assert_approx_eq::assert_approx_eq!(timer.getHours(), (nano as f64 / 3600000000000.0));
	}
}
